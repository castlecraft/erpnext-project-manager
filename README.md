# frappe-project-manager

frappe project manager - 

# Try locally

```sh

git clone https://gitlab.com/castlecraft/frappe-project-manager
cd frappe-project-manager
npm i
npm run start

```
Go to "http://localhost:4200"

Login using your google account.

# Run Tests

```sh
# Format 
npm run format 

# Lint
npm run lint

# Unit Tests
npm run test

# E2E Tests
npm run e2e
```

# Screenshots & quick overview

https://gitlab.com/castlecraft/erpnext-project-manager/wikis/home

